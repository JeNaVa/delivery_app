package com.example.deleviry_app.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.deleviry_app.R
import com.example.deleviry_app.databinding.ActivityMainBinding

class AuthActivity : AppCompatActivity() {
   private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.place_holder, HelloScreen())
            .commit()
    }
}