package com.example.deleviry_app.Presentation

import RegistrationFragment
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.deleviry_app.R
import com.example.deleviry_app.databinding.FragmentHelloScreenBinding
import com.example.deleviry_app.databinding.RegistrationFragmentBinding
import kotlin.concurrent.fixedRateTimer


class HelloScreen : Fragment() {

    private lateinit var binding: FragmentHelloScreenBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHelloScreenBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context = activity?.applicationContext

        binding.buttonLogin.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.place_holder, LoginFragment())?.commit()
        }

        binding.buttonReg.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.place_holder, RegistrationFragment())?.commit()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = HelloScreen()
    }
}