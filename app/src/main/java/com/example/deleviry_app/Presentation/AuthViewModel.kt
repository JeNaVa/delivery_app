package com.example.deleviry_app.Presentation

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.deleviry_app.Data.Auth.AuthImpl
import com.example.deleviry_app.Data.Users
import com.example.deleviry_app.Domain.Auth.AddUserUseCase
import com.example.deleviry_app.Domain.Auth.LoginUserUseCase

class AuthViewModel(con: Context?) : ViewModel() {

    private val repository = AuthImpl

    private val addUserUseCase = AddUserUseCase(repository)
    private val loginUserUseCase = LoginUserUseCase(repository)

    val context = con

    fun addUser(email: String, password: String) {
        if (validateInput(email, password))
        {
            val user = Users(null, "", "", "user", password, email)
            if (context != null) {
                Thread {
                    addUserUseCase.addUser(user, context)
                }.start()
            }
        }
    }

    fun loginUser(email: String, password: String) {
        if (validateInput(email, password)) {
            if (context != null) {
                Thread {
                    loginUserUseCase.loginUser(context, email, password)
                }.start()
            }
        }
    }


    fun validateInput(email: String, password: String) : Boolean {

        if (!email.contains('@')) {
            val toast = Toast.makeText(context, "Неправильный EMAIL", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }
        else if (password.isBlank()) {
            val toast = Toast.makeText(context, "Строка PASSWORD пуста", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }
        return true
    }

}