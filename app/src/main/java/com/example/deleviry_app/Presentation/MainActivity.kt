package com.example.deleviry_app.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.deleviry_app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.place_holder, MainFragment())
            .commit()
    }
}