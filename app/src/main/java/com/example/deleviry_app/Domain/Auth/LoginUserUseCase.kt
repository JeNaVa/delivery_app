package com.example.deleviry_app.Domain.Auth

import android.content.Context

class LoginUserUseCase(private var Auth: Auth) {
    fun loginUser(context: Context, email: String, password: String) {
        Auth.loginUser(context, email, password)
    }
}