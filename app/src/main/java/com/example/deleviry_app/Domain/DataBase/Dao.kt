package com.example.deleviry_app.Domain.DataBase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.deleviry_app.Data.Users
import kotlinx.coroutines.flow.Flow


@Dao
interface Dao {
    @Insert
    fun insertUser(item: Users)
    @Query("SELECT * FROM Users")
    fun getAllUserData(): Flow<List<Users>>
    @Query("SELECT password FROM Users WHERE email = :email")
    fun getUserPassword(email: String): String


}