package com.example.deleviry_app.Domain.User

import android.media.Image
import com.example.deleviry_app.Data.Users

interface User {

    fun setUsername(name: String, user: Users)

    fun addCard(number: String, expDate: String, cvv: String)

    fun addAddress(address: String)

    fun setProfilePicture(image: Image)

}