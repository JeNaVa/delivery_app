package com.example.deleviry_app.Domain.Auth

import android.content.Context
import com.example.deleviry_app.Data.Users

class AddUserUseCase(private val Auth: Auth) {
    fun addUser(user: Users, context: Context) {
        Auth.addUser(user, context)
    }
}