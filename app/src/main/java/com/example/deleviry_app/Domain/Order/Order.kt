package com.example.deleviry_app.Domain.Order

interface Order {

    fun setDeliveryTime()

    fun deleteOrderItem()

    fun addOrderItem()

    fun changeItemCount()

    fun makeOrder()

    fun setPaymentType()

    fun setCard()

    fun setAdress()

}