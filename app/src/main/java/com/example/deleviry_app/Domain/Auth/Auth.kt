package com.example.deleviry_app.Domain.Auth

import android.content.Context
import com.example.deleviry_app.Data.Users


interface Auth {

    fun addUser(user: Users, context: Context)

    fun loginUser(context: Context, email: String, password: String)

}