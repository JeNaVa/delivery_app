package com.example.deleviry_app.Domain.Cart

interface Cart {

    fun addCartItem()

    fun deleteCartItem()

    fun addCount()

    fun removeCount()

    fun placeAnOrder()

}