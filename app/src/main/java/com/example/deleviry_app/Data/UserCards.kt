package com.example.deleviry_app.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserCard")
data class UserCards(
    @PrimaryKey(autoGenerate = false)
    var id: Int,
    @ColumnInfo(name = "cardNumber")
    var cardNumber: String,
    @ColumnInfo(name = "cardDate")
    var cardDate: String,
    @ColumnInfo(name = "cardCVV")
    var cardCVV: String,
)
