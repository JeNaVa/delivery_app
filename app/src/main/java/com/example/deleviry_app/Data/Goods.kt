package com.example.deleviry_app.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Goods")
data class Goods(
    @PrimaryKey(false)
    var id: Int,
    @ColumnInfo("name")
    var name: String,
    @ColumnInfo("price")
    var price: Float,
    @ColumnInfo("image")
    var image: String
)
