package com.example.deleviry_app.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity("GoodCount")
data class GoodCount(
    @PrimaryKey(false)
    var id: Int,
    @ColumnInfo("count")
    var count: Int,
)
