package com.example.deleviry_app.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity("OrderGoods")
data class OrderGoods(
    @PrimaryKey(false)
    var orderId: Int,
    @ColumnInfo("good_id")
    var goodId: Int,
    @ColumnInfo("good_count")
    var goodCount: Int,
    )