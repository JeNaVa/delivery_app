package com.example.deleviry_app.Data.Auth

import android.content.Context
import android.content.Intent
import android.os.Looper
import android.widget.Toast
import com.example.deleviry_app.Data.MainDb
import com.example.deleviry_app.Data.Users
import com.example.deleviry_app.Domain.Auth.Auth
import com.example.deleviry_app.Presentation.MainActivity

object AuthImpl : Auth {
    override fun addUser(user: Users, context: Context) {
        MainDb.getDb(context).getDao().insertUser(user)
    }

    override fun loginUser(context: Context, email: String, password: String) {
        val userPassword = MainDb.getDb(context).getDao().getUserPassword(email)
        if (userPassword == password) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        } else {
            val handler = android.os.Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(context, "Неверный EMAIL или PASSWORD!", Toast.LENGTH_SHORT).show()
            }
        }
    }


}


