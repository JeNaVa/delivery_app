package com.example.deleviry_app.Data

import android.content.Context
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database (entities =
        [Users::class,
        Goods::class,
        Order::class,
        OrderGoods::class,
        GoodCount::class,
        UserCards::class
        ],
    version = 3)
abstract class MainDb : RoomDatabase() {
    abstract fun getDao(): com.example.deleviry_app.Domain.DataBase.Dao

    companion object {

        @Volatile
        private var INSTANCE: MainDb? = null

        val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("ALTER TABLE Users ADD COLUMN email TEXT NOT NULL DEFAULT ''")
            }
        }

        fun getDb(context: Context): MainDb {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDb::class.java,
                    "app_database"
                ).addMigrations(MIGRATION_2_3).build()
                INSTANCE = instance
                instance
            }
        }

    }
}