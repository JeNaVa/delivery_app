package com.example.deleviry_app.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Order")
data class Order(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    @ColumnInfo(name = "user_id")
    var userId: Int,
    @ColumnInfo(name = "address")
    var address: String,
    @ColumnInfo(name = "goods")
    var goods: String,
    @ColumnInfo(name = "role")
    var role: String,
    @ColumnInfo(name = "paymentType")
    var paymentType: Int,
    @ColumnInfo(name = "deliveryTime")
    var deliveryTime: String,
    @ColumnInfo(name = "price")
    var price: Float,
)